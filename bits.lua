--[ GIBBON JOYEUX ]--
-- bits.lua

--[[
--
-- API:
--
-- . _init()
-- . _update()
-- . _draw()
--
--   . size( w, h, scale )				set window format
--   . save( path )						save canvas to .png or .tga
--   . record()							start recording
--   . saverecord( path_format )		save recording to .png or .tga files
--   . time()							get time in seconds
--   . timemode( mode, [fps] )			set time mode
--   . quit()							quit program
--
-- . DRAWING FUNCTIONS:
--   . clear( [color] )
--   . colors( color_list )				set colors list
--   . color(c)							set color
--   . point( x, y, [c] )				draw point
--   . line( x1 y1, x2, y2, [c] )		draw line
--   . rect( x, y, w, h, [c] )			draw rectangle
--   . rectf( x, y, w, h, [c] )			draw filled rectangle
--   . circ( x, y, r, [c] )				draw circle
--   . circf( x, y, r, [c] )			draw filled circle
--   . ellipse( x, y, rw, rh, [c] )		draw ellipse
--   . ellipsef( x, y, rw, rh, [c] )	draw filled ellipse
--   . fpoly( x, y, [...] )				draw free polygon
--   . fpolyf( x, y, [...] )			draw filled free polygon
--   . poly( x, y, v, r, [a], [c] )		draw regular polygon
--   . polyf( x, y, v, r, [a], [c] )	draw filled regular polygon
--
-- . SPRITESHEET FUNCTIONS
--   - INDEX MODE: draw sprites from index
--     . sheet = sprs( path, "index", w, h )
--     . sheet:add( x, y, i, w, h )
--   - TILES MODE: draw sprites from x y coordinates
--     . sheet = sprs( path, "tiles", w, h )
--     . sheet:add( x, y, tile_x, tile_y, w, h )
--   - FREE MODE: 
--     . sheet = sprs( path, "free", step_x, step_y )
--     . sheet:add( x, y, spr_x, spr_y, w, h )
--   - ALL MODES:
--     . sheet:get( x, y )				get spritesheet pixel
--     . sheet:set( x, y, color )		set spritesheet pixel
--     . sheet:update()					update spritesheet modified pixels
--
-- . CANVAS FUNCTIONS:
--   . cnv = newcanvas( [w, h] )		create new canvas
--   . cnv:readpixels()					load canvas data for cnv:get()
--   . cnv:closepixels()				release canvas data
--   . cnv:get( x, y )					get pixel color
--   . cnv:draw( [x, y] )				draw canvas
--   . cnv:save( path )					save canvas to .png or .tga
--
-- . MATH FUNCTIONS:
--   . cos, acos, sin, asin, tan, atan, ceil, flr, abs, min, max
--     pow, exp, log, sgn
--   . noise( x, [y], [z], [a] )
--   . rnd() or rnd( max ) or rnd( min, max )
--   . frnd() or frnd( max ) or frnd( min, max )	floored random
--   . mid( a, b, c )								acts like constrain
--   . map( x, start1, stop1, start2, stop2 )
--   . mapc( x, start1, stop1, start2, stop2 )
--
-- . TABLE FUNCTIONS:
--   . add( table, item )				add item to table
--   . del( table, item )				remove item from table and return it
--   . sort( table, [function] )		sort table
--
--]]

bits = {
	last_batch = nil,
	time = {
		frame = 0,
		constant = false,
		fps = 60
	},
	format = {
		w = 128,
		h = 128,
		scale = 6
	},
	canvas = {
		base = nil,
		active = nil
	},
	record = {
	},
	colors = {
		{ 0, 0, 0 },
		{ 0x1D / 255, 0x2B / 255, 0x53 / 255 },
		{ 0x7E / 255, 0x25 / 255, 0x53 / 255 },
		{ 0x00 / 255, 0x87 / 255, 0x51 / 255 },
		{ 0xAB / 255, 0x52 / 255, 0x36 / 255 },
		{ 0x5F / 255, 0x57 / 255, 0x4F / 255 },
		{ 0xC2 / 255, 0xC3 / 255, 0xC7 / 255 },
		{ 0xFF / 255, 0xF1 / 255, 0xE8 / 255 },
		{ 0xFF / 255, 0x00 / 255, 0x4D / 255 },
		{ 0xFF / 255, 0xA3 / 255, 0x00 / 255 },
		{ 0xFF / 255, 0xEC / 255, 0x27 / 255 },
		{ 0x00 / 255, 0xE4 / 255, 0x36 / 255 },
		{ 0x29 / 255, 0xAD / 255, 0xFF / 255 },
		{ 0x83 / 255, 0x76 / 255, 0x9C / 255 },
		{ 0xFF / 255, 0x77 / 255, 0xA8 / 255 },
		{ 0xFF / 255, 0xCC / 255, 0xAA / 255 }
	}
}

--------------------------------------------------------------------------------
-- LOVE FUNCTIONS
--------------------------------------------------------------------------------

function		love.load()
	-- INIT LOVE
	love.graphics.setLineStyle( "rough" )
	love.graphics.setLineWidth( 1 )
	-- INIT BITS
	size( bits.format.w, bits.format.h, bits.format.scale )
	-- INIT USER
	if _init then _init() end
end

function		love.update( dt )
	if bits.time.constant then
		dt = 1 / bits.time.fps
	end
	if _update then _update( dt ) end
	bits.time.frame = bits.time.frame + 1
end

function		love.draw()
	local		r, g, b, a

	-- CALL USER _draw()
	setcanvas( bits.canvas.base )
	if _draw then _draw() end
	-- FLUSH LAST BATCH
	spritesheet_flush()
	r, g, b, a = love.graphics.getColor()
	love.graphics.setColor( 1, 1, 1, 1 )
	love.graphics.setCanvas()
	love.graphics.draw( bits.canvas.base.cnv, 0, 0, 0, bits.format.scale,
	bits.format.scale )
	love.graphics.setColor( r, g, b, a )
end

--------------------------------------------------------------------------------
-- COLOR FUNCTIONS
--------------------------------------------------------------------------------

local function			get_matching_color( r, g, b, a )
	if a ~= 1 then return -1 end
	r = flr( r * 255 )
	g = flr( g * 255 )
	b = flr( b * 255 )
	for i, col in pairs( bits.colors ) do
		if flr( col[ 1 ] * 255 ) == r
		and flr( col[ 2 ] * 255 ) == g
		and flr( col[ 3 ] * 255 ) == b then
			return i - 1
		end
	end
	return 0
end

--------------------------------------------------------------------------------
-- FORMAT FUNCTIONS
--------------------------------------------------------------------------------

time = love.timer.getTime
quit = love.event.quit

function		timemode( mode, fps )
	-- CONSTANT MODE (for recording)
	if mode == "constant" then
		bits.time.constant = true
		bits.time.fps = fps or 60
		time = function () return bits.time.frame / bits.time.fps end
	-- REAL TIME MODE
	else
		bits.time.constant = false
		time = love.timer.getTime
	end
end

function		size( w, h, scale )
	local		screen_w, screen_h

	if scale == nil then scale = 1 end
	screen_w = w * scale
	screen_h = h * scale
	love.window.setMode( screen_w, screen_h )
	bits.canvas.base = newcanvas( w, h )--love.graphics.newCanvas( w, h )
	bits.canvas.base.cnv:setFilter( "nearest", "nearest" )
	bits.canvas.active = bits.canvas.base
	bits.format.w = w
	bits.format.h = h
	bits.format.scale = scale
end

function		save( path )
	local		ext

	ext = string.sub( path, #path - 2 )
	spritesheet_flush()
	love.graphics.setCanvas()
	bits.canvas.active.cnv:newImageData():encode( ext, path )
	love.graphics.setCanvas( bits.canvas.active.cnv )
end

function		record()
	spritesheet_flush()
	love.graphics.setCanvas()
	add(bits.record, bits.canvas.active.cnv:newImageData())
	love.graphics.setCanvas( bits.canvas.active.cnv )
end

function		saverecord( path )
	local		file_path
	local		ext

	ext = string.sub( path, #path - 2 )
	for i, data in pairs(bits.record) do
		file_path = string.format(path, i)
		data:encode( ext, file_path )
	end
	bits.record = {}
end

--------------------------------------------------------------------------------
-- DRAWING FUNCTIONS
--------------------------------------------------------------------------------

function		color( c )
	love.graphics.setColor( bits.colors[ 1 + ( c or 0 ) % #bits.colors ] )
end

function		colors( color_list )
	bits.colors = color_list
end

function		clear( c )
	if c then
		love.graphics.clear( bits.colors[ 1 + c % #bits.colors ] )
	else
		love.graphics.clear()
	end
end

function		point( x, y, c )
	spritesheet_flush()
	if c ~= nil then color( c ) end
	love.graphics.points( x, y )
end

function		line( x1, y1, x2, y2, c )
	spritesheet_flush()
	if c ~= nil then color( c ) end
	love.graphics.line( x1, y1, x2, y2 )
end

function		rect( x, y, w, h, c )
	spritesheet_flush()
	if c ~= nil then color( c ) end
	love.graphics.rectangle( "line", x, y, w, h )
end

function		rectf( x, y, w, h, c )
	spritesheet_flush()
	if c ~= nil then color( c ) end
	love.graphics.rectangle( "fill", x, y, w, h )
end

function		circ( x, y, r, c )
	spritesheet_flush()
	if c ~= nil then color( c ) end
	love.graphics.circle( "line", x, y, r )
end

function		circf( x, y, r, c )
	spritesheet_flush()
	if c ~= nil then color( c ) end
	love.graphics.circle( "fill", x, y, r )
end

function		ellipse( x, y, rw, rh, c )
	spritesheet_flush()
	if c ~= nil then color( c ) end
	love.graphics.ellipse( "line", x, y, rw, rh )
end

function		ellipsef( x, y, rw, rh, c )
	spritesheet_flush()
	if c ~= nil then color( c ) end
	love.graphics.ellipse( "fill", x, y, rw, rh )
end

function		fpoly( ... )
	spritesheet_flush()
	love.graphics.polygon( "line", unpack( arg ) )
end

function		fpolyf( ... )
	spritesheet_flush()
	love.graphics.polygon( "fill", unpack( arg ) )
end

function		poly( x, y, v, r, a, c )
	local		slice
	local		points

	spritesheet_flush()
	if c ~= nil then color( c ) end
	slice = 1 / v
	for i = 0, v + 1 do
		add( points, x + cos( slice * i + a) * r )
		add( points, y + sin( slice * i + a) * r )
	end
	love.graphics.polygon( "line", points )
end

function		polyf( x, y, v, r, a, c )
	local		slice
	local		points

	spritesheet_flush()
	if c ~= nil then color( c ) end
	if a == nil then a = 0 end
	slice = 1 / v
	for i = 0, v + 1 do
		add( points, x + cos( slice * i + a ) * r )
		add( points, y + sin( slice * i + a ) * r )
	end
	love.graphics.polygon( "fill", points )
end

--------------------------------------------------------------------------------
-- TABLE FUNCTIONS
--------------------------------------------------------------------------------

add = table.insert
sort = table.sort

function	del( tab, obj )
	for i, item in pairs(tab) do
		if item == obj then
			table.remove(tab, i)
			return
		end
	end
end

--------------------------------------------------------------------------------
-- MATH FUNCTIONS
--------------------------------------------------------------------------------

PI = math.pi
PI2 = math.pi * 2
abs = math.abs
min = math.min
max = math.max
ceil = math.ceil
flr = math.floor
pow = math.pow
log = math.log
exp = math.exp
sqrt = math.sqrt
noise = love.math.noise
rnd = love.math.random
function		frnd( i, j ) return flr( rnd( i, j ) ) end
function		cos( i ) return math.cos( i * PI2 ) end
function		acos( i ) return math.acos( i * PI2 ) end
function		sin( i ) return math.sin( i * PI2 ) end
function		asin( i ) return math.asin( i * PI2 ) end
function		tan( i ) return math.tan( i * PI2 ) end
function		atan( i ) return math.atan( i * PI2 ) end
function		atan2( i, j ) return math.atan2( i * PI2, j * PI2 ) end

function		mid( lower, x, higher )
	return min( max( x, lower ), higher )
end

function		sgn( x )
	return ( x < 0 ) and -1 or ( ( x > 0 ) and 1 or 0 )
end

function		map( x, start1, stop1, start2, stop2 )
	return (start2) + ( ( x - start1 ) / ( stop1 - start1 ) )
	* ( stop2 - start2 )
end

function		mapc( x, start1, stop1, start2, stop2 )
	return mid( start2, map( x, start1, stop1, start2, stop2 ), stop2 )
end

--------------------------------------------------------------------------------
-- SPRITESHEET FUNCTIONS
--------------------------------------------------------------------------------

local meta_sprs = {}
meta_sprs.__index = meta_sprs

--------------------------------------------------
-- PRIVATE FUNCTIONS
--------------------------------------------------

function		spritesheet_flush( active )
	local		r, g, b, a

	if bits.last_batch and bits.last_batch ~= active then
		r, g, b, a = love.graphics.getColor()
		love.graphics.setColor( 1, 1, 1, 1 )
		love.graphics.draw( bits.last_batch )
		bits.last_batch:clear()
		love.graphics.setColor( r, g, b, a )
	end
	bits.last_batch = active
end

local function	spritesheet_retrieve_quads( obj, w, h, img_w, img_h )
	local		i
	local		x, y
	local		quads

	i = 0
	quads = {}
	for y = 0, obj.tiles_y - 1 do
		for x = 0, obj.tiles_x - 1 do
			quads[ i ] = love.graphics.newQuad( x * w, y * h, w, h, img_w,
			img_h )
			i = i + 1
		end
	end
	obj.quads = quads
end

local function	spritesheet_create( data, img, batch, mode, p1, p2 )
	local		obj
	local		img_w, img_h

	-- HANDLE MODE
	if mode == "index" then mode = 0		-- index mode
	elseif mode == "tiles" then mode = 1	-- tiles mode
	else mode = 2 end						-- free mode
	-- CREATE TABLE
	img_w, img_h = img:getDimensions()
	obj = {
		data = data,
		img = img,
		batch = batch,
		mode = mode,
		tile_w = p1,
		tile_h = p2,
		tiles_x = flr( img_w / p1 ),
		tiles_y = flr( img_h / p2 )
	}
	-- CREATE QUADS
	if mode < 2 then
		spritesheet_retrieve_quads( obj, p1, p2, img_w, img_h )
	else
		obj.quads = {}
	end
	-- RETURN TABLE
	setmetatable( obj, meta_sprs )
	return obj
end

--------------------------------------------------
-- PUBLIC FUNCTIONS
--------------------------------------------------

-- newsprs( path, "index", tile_width, tile_height )
-- newsprs( path, "tiles", tile_width, tile_height )
-- newsprs( path, "free", step_x, step_y )
function		sprs( path, mode, p1, p2 )
	local		data
	local		img
	local		batch

	data = love.image.newImageData( path )
	img = love.graphics.newImage( data )
	batch = love.graphics.newSpriteBatch( img )
	return spritesheet_create( data, img, batch, mode or "index", p1, p2 )
end

--------------------------------------------------
-- SPRITESHEET METATABLE
--------------------------------------------------

local function	spritesheet_draw_index( self, i, w, h, x, y,
				flip_x, flip_y )
	local		quad_x, quad_y
	local		spr_x, spr_y
	local		scale_x, scale_y
	local		off_x, off_y

	-- FLIP
	scale_x, scale_y = 1, 1
	off_x, off_y = 0, 0
	if flip_x then
		scale_x = -1
		off_x = self.tile_w
	end
	if flip_y then
		scale_y = -1
		off_y = self.tile_h
	end
	-- DRAW
	quad_y = flip_y and ( h - 1 ) or 0
	for spr_y = 0, h - 1 do
		quad_x = flip_x and ( w - 1 ) or 0
		for spr_x = 0, w - 1 do
			quad = self.quads[ i + quad_x + quad_y * self.tiles_x ]
			self.batch:add( quad, x + spr_x * self.tile_w + off_x,
			y + spr_y * self.tile_h + off_y,
			0, scale_x, scale_y )
			quad_x = quad_x + scale_x
		end
		quad_y = quad_y + scale_y
	end
end

--------------------------------------------------------------------------------
local function	spritesheet_draw_free( self, x, y, spr_x, spr_y, spr_w, spr_h,
				flip_x, flip_y )
	local		quad_name
	local		quad
	local		off_x, off_y

	scale_x, scale_y = 1, 1
	off_x, off_y = 0, 0
	if flip_x then
		scale_x = -1
		off_x = spr_w
	end
	if flip_y then
		scale_y = -1
		off_y = spr_h
	end

	quad_name = spr_x .. ":" .. spr_y .. ":" .. spr_w .. ":" .. spr_h
	quad = self.quads[ quad_name ]
	if quad == nil then
		quad = love.graphics.newQuad( spr_x * self.tile_w, spr_y * self.tile_h,
		spr_w * self.tile_w, spr_h * self.tile_h,
		self.img:getWidth(), self.img:getHeight() )
		self.quads[ quad_name ] = quad
	end
	self.batch:add( quad, x + off_x, y + off_y, 0, scale_x, scale_y )
end

-- spr( x, y, spri, [w, h,] [flipx, flipy,] [r, sz, sy, ox, oy, kz, ky] )		-- index mode
-- spr( x, y, sprx, spry, [w, h,] [flipx, flipy,] [r, sz, sy, ox, oy, kz, ky] )	-- tiles mode
-- spr( x, y, sprx, spry, w, h, [flipx, flipy,] [r, sz, sy, ox, oy, kz, ky] )	-- free mode
meta_sprs.add =		function( self, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10,
					p11 )
	local			i
	local			r, g, b, a


	-- BITS
	spritesheet_flush( self.batch )
	r, g, b, a = love.graphics.getColor()
	love.graphics.setColor( 1, 1, 1, 1 )
	-- DRAW
	if self.mode == 0 then		-- index mode
		spritesheet_draw_index( self, p3, p4 or 1, p5 or 1, p1, p2,
		p6, p7 )
	elseif self.mode == 1 then	-- tiles mode
		i = p4 * self.tiles_x + p3
		spritesheet_draw_index( self, i, p5 or 1, p6 or 1, p1, p2,
		p7, p8 )
	else						-- free mode
		spritesheet_draw_free( self, p1, p2, p3, p4, p5, p6,
		p7, p8 )
	end
	-- RESET COLOR
	love.graphics.setColor( r, g, b, a )
end

meta_sprs.get =		function( self, x, y )
	local			r, g, b, a

	r, g, b, a = self.data:getPixel( x, y )
	return get_matching_color( r, g, b, a )
end

meta_sprs.set = 	function( self, x, y, c )
	local			col

	col = bits.colors[ 1 + (c or 0 ) % #bits.colors ]
	self.data:setPixel( x, y, col[ 1 ], col[ 2 ], col[ 3 ] )
end

meta_sprs.update =	function( self )
	self.img:replacePixels( self.data )
end

--------------------------------------------------------------------------------
-- CANVAS FUNCTIONS
--------------------------------------------------------------------------------

meta_cnv = {}
meta_cnv.__index = meta_cnv

meta_cnv.draw =			function( self, x, y )
	love.graphics.draw( self.cnv, x, y )
end

meta_cnv.readpixels =	function( self )
	self.data = self.cnv:newImageData()
end

meta_cnv.closepixels =	function( self )
	self.data = nil
end

meta_cnv.get =			function( self, x, y )
	local				r, g, b, a

	r, g, b, a = self.data:getPixel( x, y )
	return get_matching_color( r, g, b, a )
end

meta_cnv.save =			function( self, path )
	local				ext

	ext = string.sub( path, #path - 2 )
	spritesheet_flush()
	love.graphics.setCanvas()
	self.cnv:newImageData():encode( ext, path )
	love.graphics.setCanvas( bits.canvas.active.cnv )
end

function		newcanvas( w, h )
	local		cnv

	if w == nil then w = bits.format.w end
	if h == nil then h = bits.format.h end
	cnv = {
		cnv = love.graphics.newCanvas( w, h ),
		data = nil
	}
	setmetatable( cnv, meta_cnv )
	return cnv
end

function		setcanvas( cnv )
	spritesheet_flush()
	bits.canvas.active = cnv or bits.canvas.base
	love.graphics.setCanvas( bits.canvas.active.cnv )
end

--------------------------------------------------------------------------------
-- ANIMATION FUNCTIONS
--------------------------------------------------------------------------------

function		wiggle( speed, amp, offset )
	return ((1 + sin((time() + (offset or 0)) * speed)) / 2) * (amp or 1)
end

function		frameloop( speed, num, offset )
	return flr((time() + (offset or 0)) * speed) % num
end

function		framepingpong( speed, num, offset )
	local		f
	local		amp

	amp = num * 2 - 2
	f = flr((time() + (offset or 0)) * speed) % amp
	return (f < num) and f or amp - f
end
