--[ GIBBON JOYEUX ]--
-- main.lua

require "bits"

function		_init()
	size( 128, 72, 6 )
	cnv = newcanvas()
	sheet = sprs( "spritesheet.png", "index", 8, 8 )
	sheet:set( 0, 0, 9 )
	sheet:set( 1, 0, 9 )
	sheet:set( 0, 1, 9 )
	sheet:update()
	i = 0
end

function		_update()
end

function		_draw()
	local		x, y
	local		base_y

	clear()
	setcanvas( cnv )
		clear( 2 )
		for x = 0, 128 do
			base_y = 72 / 2
			+ sin( time() + x / 50 ) * 5
			+ sin( time() / 3 + x / 30 ) * 7
			+ sin( time() / 5 + x / 50 ) * 3
			+ sin( time() / 8 + x / 60 ) * 10
			for y = base_y, 72 do
				c = flr( map( y, base_y, 72, 0, 16 ) + x / 20 )
				color( c )
				point( x, y )
			end
		end
		i = i + 1
		sheet:add( 30, 50, 0 )
	setcanvas()
	cnv:draw()
end
