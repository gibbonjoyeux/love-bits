
# bits

**bits** is a small love2d library made to ease pixelart games or generative art making.

It is highly inspired by **pico-8** but does not implies any restriction (expect the color indexing).

## FEATURES

- **indexed colors** (pico-8 palette by default but you can use your own palette)
- easy **sprites & spritesheets** handling (with different modes)
- discrete use of love **spritebatch** into spritesheets for optimization
- easy access to **math functions**
- easily **scale** your project window for more readibility (essential for pixelart)
- easily save main canvas or user canvas to **.png** or **.tga** images
- love functions are, of course, still available

# USE IT

```

require "bits.lua"

function	_init()
	-- Set window size to 128x128px
	-- With a scale of 4 (512x512px)
	size( 128, 128, 4 )
	
	-- Load a spritesheet with the "index" mode
	-- Spritesheet sprites are tiles of 8x8px
	sheet = sprs( "character_spritesheet.png", "index", 8, 8 )

	-- init character animation
	i = 0
end

function	_update( dt )
	-- loop character animation on 12 frames
	i = ( i + 1 ) % 12
end

function	_draw()
	-- add the corresponding character frame at 30x:30y
	sheet:add( 30, 30, i )
end

```

# API

## MAIN
- `_init()`			Use instead of love.load()
- `_update( dt )`	Use instead of love.update( dt )
- `_draw()`			Use instead of love.draw()

## FORMAT & GENERIC
- `size( w, h, scale )`		Set window dimensions. `scale` is used to scale window without modifying main canvas size
- `save( path )`			Save main canvas to .png or .tga (.png lighter, .tga faster)
- `record()`				Start recording
- `saverecord(path_format)`	Save recording to .png or .tga files (faster than `save()` to record a long sequence of images but requires memory). The path must contain `%d` (ex: "frame\_%d.tga")
- `time()`					Get time in seconds
- `timemode( mode, [fps] )`	Set time() mode ("real" (default) means realtime will be returned, "constant" (with optional desired fps) should be used when you want to record/save your projects frames to avoid a varying delta time between frames)
- `quit()`

## DRAWING
- `clear( [color] )`				Clear canvas (if no color is provided, canvas will be transparent)
- `colors( color_list )`			Set indexed color list (ex: `{ {1,0,0}, {0,1,0}, {0,0,1} }`)
- `color( [c] )`					Set drawing color
- `point( x, y, [c] )`				Draw point
- `line( x1 y1, x2, y2, [c] )`		Draw line
- `rect( x, y, w, h, [c] )`			Draw rectangle
- `rectf( x, y, w, h, [c] )`		Draw filled rectangle
- `circ( x, y, r, [c] )`			Draw circle
- `circf( x, y, r, [c] )`			Draw filled circle
- `ellipse( x, y, rw, rh, [c] )`	Draw ellipse
- `ellipsef( x, y, rw, rh, [c] )`	Draw filled ellipse
- `fpoly( x, y, [...] )`			Draw free polygon
- `fpolyf( x, y, [...] )`			Draw filled free polygon
- `poly( x, y, v, r, [a], [c] )`	Draw regular polygon
- `polyf( x, y, v, r, [a], [c] )`	Draw filled regular polygon

## SPRITESHEET

#### About spritesheets:
There are **3 modes** to create and use spritesheets.

The `index` and `tiles` modes split the spritesheets into sprites of given dimensions.
With the `index` mode, inspired by **pico-8**, sprites are loaded from left to right, up to bottom. You only need to provided the sprite index (starts with 0) to draw it.

With the `tiles` mode, you need to provide the sprite `x` & `y` coordinates (sprites coordinates are not in pixels but in sprites).

In both `index` and `tiles` mode, when you draw a sprite, a `width` and a `height` can be given to draw bigger sprites made of unit sprites.

Finally, the `free` mode loads sprites only when you require them (drawn sprites are then kepts for further use). At the `free` mode spritesheets creation, you can provide the units (step) used to get sprites (1px by 1px by default, but you can use something like 8px by 8px).

### INDEX MODE: draw sprites from index
- `sheet = sprs( path, "index", sprites_w, sprites_h )`	Create indexed spritesheet
- `sheet:add( x, y, index, w, h, flip_x, flip_y )`		Draw indexed spritesheet sprite

### TILES MODE: draw sprites from x y coordinates
- `sheet = sprs( path, "tiles", w, h )`						Create tiled spritesheet
- `sheet:add( x, y, tile_x, tile_y, w, h, flip_x, flip_y )`	Draw tiled spritesheet sprite

### FREE MODE: 
- `sheet = sprs( path, "free", step_x, step_y )`			Create free spritesheet
- `sheet:add( x, y, spr_x, spr_y, w, h, flip_x, flip_y )`	Draw free spritesheet sprite

### ALL MODES:
- `sheet:get( x, y )`				Get spritesheet pixel
- `sheet:set( x, y, color )`		Set spritesheet pixel
- `sheet:update()`					Update spritesheet modified pixels

## CANVAS
- `cnv = newcanvas( [w, h] )`		Create new canvas (by default, the canvas gets main canvas dimensions)
- `cnv:readpixels()`				Load canvas data for `cnv:get()`
- `cnv:closepixels()`				Release canvas data
- `cnv:get( x, y )`					Get pixel color
- `cnv:draw( [x, y] )`				Draw canvas
- `cnv:save( path )`				Save canvas to .png or .tga (.png lighter, .tga faster)

## MATH
- `cos`, `acos`, `sin`, `asin`, `tan`, `atan`	Basic trigonometry functions. Using a full circle range of 0.0-1.0 (easier for game or graphics).
- `min`, `max`, `abs`
- `mid( a, b, c )`		Returns the middle of three numbers. Also useful for clamping.
- `flr`, `ceil`
- `rnd()`, `rnd( max )`, `rnd( min, max )`		Random functions
- `frnd()`, `frnd( max )`, `frnd( min, max )`	Floored random
- `noise( x, [y], [z], [w] )`					Generate perlin noise is 1-4 dimensions
- `map( x, start1, stop1, start2, stop2 )`		Remap x from start1-stop1 to start2-stop2
- `mapc( x, start1, stop1, start2, stop2 )`		Remapc and constrains result

## TABLE
- `add( table, item )`				Add item to table
- `del( table, item )`				Remove item from table and return it
- `sort( table, [function] )`		Sort table (with an optional sorting function)
